# @rossoorg/storage

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm]

Abstract storage adapter for Rosso.

## Installation

```bash
npm i @rossoorg/storage
```

[license]: https://gitlab.com/rosso-org/storage/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/rosso-org/storage/pipelines
[pipelines_badge]: https://gitlab.com/rosso-org/storage/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@rossoorg/storage
[npm_badge]: https://img.shields.io/npm/v/@rossoorg/storage/latest.svg
