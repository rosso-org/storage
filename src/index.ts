import { URL } from "url";

export abstract class Storage {
  public protocol: Readonly<string>;

  public constructor(protocol: string) {
    this.protocol = protocol;

    this.load = this.load.bind(this);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
  }

  /**
   * Load binary data with given identifier.
   *
   * @param url Identifier of the data.
   * @returns Binary data of the file.
   *
   * @throws InvalidUrlError
   */
  abstract load(url: URL): Promise<Buffer>;

  /**
   * Save binary data to a new location and return an identifier.
   *
   * @param buffer Binary data to be saved.
   * @return Identifier of the data.
   */
  abstract save(buffer: Buffer, path: string): Promise<URL>;

  /**
   * Delete binary data with given identifier.
   *
   *  @param url Identifier of the data.
   */
  abstract delete(url: URL): Promise<void>;
}

export class InvalidUrlError extends Error {
  public url: Readonly<URL>;

  public constructor(url: URL) {
    super(`Invalid URL (${url.href})`);
    this.url = url;
  }
}
