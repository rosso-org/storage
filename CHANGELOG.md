# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.0]

### Added

- Add `InvalidUrlError`.

### Changed

- `save` now requires `pathname`.

## 1.0.0

### Added

- Initial Release.

[unreleased]: https://gitlab.com/rosso-org/storage/compare/2.0.0...master
[2.0.0]: https://gitlab.com/rosso-org/storage/compare/1.0.0...2.0.0
